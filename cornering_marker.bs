-- cornering_marker.bs --

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"interval", 3, 100, 8},
		{"cornering", 0, 100, 50},
		{"rnd_size", 0, 100, 0},
		{"rnd_pos", 0, 100, 0},
		{"move_dir", 0, 1, 0},
		{"angle", 0, 100, 25},
		{"rnd_angle", 0, 100, 0},
		{"rp_alpha", 0, 100, 30},
		{"p_size", 0, 100, 50},
		{"p_alpha", 0, 100, 50}
	}

	pm.ja={
		"間隔", "コーナリング", "rnd_サイズ", "rnd_位置", "進行方向",
		"角度", "rnd_角度", "rp_透明度", "p_サイズ", "p_透明度"
	}

	for i in ipairs(pm) do

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		_G[string.format('param%d', i)]=setParam(pm[i])
		pm[i].v=_G[string.format('bs_param%d', i)]()

	end

	default_size=setParam(30, 0.5)
	firstDraw=true
	bs_setmode(1)

-- sub --
average={
	init=function ()
		local obj={array={}, arrayBak={}, divSum=0, divWeight=0}
		setmetatable(obj, {__index=average})
		return obj
	end
}

function average:put(num, value)

	if #self.array == 0 then
		for i=1, math.max(num, 1) do
			self.array[i]=value
		end
	else
		table.insert(self.array, value)
		table.remove(self.array, 1)
	end

	self.arrayBak={unpack(self.array)}
end

function average:cut(num)
	if #self.array < 1 then return end
	if num > 0 then
		for i=1, math.min(num, #self.array-1) do
			table.remove(self.array, i)
		end
	end
end

function average:sum(mode, weight)

	if mode < 0 or mode > 2 then
		return nil
	end

	self.divSum, self.divWeight=0, 0

	for i, v in ipairs(self.array) do

		local _weight=1

		if mode == 1 then
			_weight=i
		elseif mode == 2 then
			_weight=i^weight
		end

		self.divSum=self.divSum+v*_weight
		self.divWeight=self.divWeight+_weight
	end

	self.divSum=self.divSum/self.divWeight
	self.array={unpack(self.arrayBak)}

	return self.divSum
end

-- main --
function main(x, y, p)

	local wMin, wMax=bs_width_min(), bs_width_max()
	local pp=1

	if pm[9].v > 0 then
		pp=p^(4*pm[9].v/100)*(1-wMin/wMax)+wMin/wMax
	end

	local w=wMax*pp

	if firstDraw then
		cornerSmooth=average.init()
		cornerLv=0
		counter=0
		lastX, lastY, lastP=x, y, pp
		lastRad={0, 0}
	end

	local skipFirstDraw=math.min(100, math.floor(wMax/4+0.5))*0.65
	local dx, dy=bs_dir()
	local rad={bs_atan(lastX-x, lastY-y), bs_atan(x-lastX, y-lastY), bs_atan(dx, dy)}

	if counter > skipFirstDraw then
		cornerSmooth:put(skipFirstDraw*1.5, math.abs(math.sin(lastRad[3]-rad[3]))/0.1)
		cornerSmooth:cut(math.floor(math.max(#cornerSmooth-(counter-skipFirstDraw-1), 0)*(1-p)))
		cornerLv=math.min(cornerSmooth:sum(2, 1-p)*pm[2].v/100, 1)^2
	end

	w=w+w/(2.5+cornerLv*p)*cornerLv

	local distance=bs_distance(lastX-x, lastY-y)
	local interval=w*pm[1].v/100

	if counter > skipFirstDraw then
		interval=interval-interval*math.min(math.min(cornerLv^(1-p), 0.95), cornerLv/0.1)
	else
		interval=0
	end

	interval=math.max(interval*(lastP-pp+1), 1)

	if not firstDraw then
		if distance < interval then
			return 0
		end
	end

	local midX, midY=x-distance%interval*dx, y-distance%interval*dy
	local angle=math.pi*pm[6].v/100

	if pm[5].v == 1 then
		angle=angle+rad[3]
	end

	for i=0, math.max(math.floor(distance/interval*(1-pp)), 0) do

		local ww=w*math.random(100-pm[3].v, 100)/100
		angle=angle+bs_grand(0, math.pi*2*pm[7].v/100)

		local r, g, b=bs_fore()
		local a=255*bs_opaque()

		local limitT=0.95

		if p > limitT then
			a=(1-(p-limitT)/(1-limitT)*0.9)^(pm[8].v/100)*a
		end

		if pm[10].v > 0 then
			if p < limitT then
				a=(1/limitT*p)^(pm[10].v/100*8)*a
			end
		end

		a=math.min(255, a+a*(1-a/255)*cornerLv)

		local wLimit=1

		if w < wLimit then
			a=a*(pp^(wLimit-w/wLimit))
			ww=1
		end

		local rAngle=bs_grand(0, math.pi*2)
		local rDist=w*math.random(0, pm[4].v)/100

		local xx, yy=midX+math.cos(rAngle)*rDist, midY+math.sin(rAngle)*rDist
		local ddx, ddy=math.cos(rad[1]), math.sin(rad[1])

		xx=xx+ddx*interval*i
		yy=yy+ddy*interval*i

		if counter >= skipFirstDraw then
			bs_ellipse(xx, yy, ww*0.65, ww, angle, r, g, b, a)
		end
	end

	lastX, lastY, lastP=midX, midY, pp
	lastRad={bs_atan(lastX-x, lastY-y), bs_atan(x-lastX, y-lastY), bs_atan(dx, dy)}
	counter=counter+1
	firstDraw=false

	return 1
end
