-- cornering_meshPencil.bs --

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"interval", 3, 100, 50},
		{"cornering", 00, 100, 20},
		{"rnd_size", 0, 100, 0},
		{"rnd_pos", 0, 100, 0},
		{"move_dir", 0, 1, 0},
		{"angle", 0, 100, 20},
		{"rnd_angle", 0, 100, 2},
		{"rp_alpha", 0, 100, 70},
		{"p_size", 0, 100, 50},
		{"p_alpha", 0, 100, 25}
	}

	pm.ja={
		"間隔", "コーナリング", "rnd_サイズ", "rnd_位置", "進行方向",
		"角度", "rnd_角度", "rp_透明度", "p_サイズ", "p_透明度"
	}

	for i in ipairs(pm) do

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		_G[string.format('param%d', i)]=setParam(pm[i])
		pm[i].v=_G[string.format('bs_param%d', i)]()

	end

	default_size=setParam(50, 0.15)
	firstDraw=true
	multipleMode=true

	bs_setmode(1)

-- sub --
average={
	init=function ()
		local obj={array={}, arrayBak={}, divSum=0, divWeight=0}
		setmetatable(obj, {__index=average})
		return obj
	end
}

function average:put(num, value)

	if #self.array == 0 then
		for i=1, math.max(num, 1) do
			self.array[i]=value
		end
	else
		table.insert(self.array, value)
		table.remove(self.array, 1)
	end

	self.arrayBak={unpack(self.array)}
end

function average:cut(num)
	if #self.array < 1 then return end
	if num > 0 then
		for i=1, math.min(num, #self.array-1) do
			table.remove(self.array, i)
		end
	end
end

function average:sum(mode, weight)

	if mode < 0 or mode > 2 then
		return nil
	end

	self.divSum, self.divWeight=0, 0

	for i, v in ipairs(self.array) do

		local _weight=1

		if mode == 1 then
			_weight=i
		elseif mode == 2 then
			_weight=i^weight
		end

		self.divSum=self.divSum+v*_weight
		self.divWeight=self.divWeight+_weight
	end

	self.divSum=self.divSum/self.divWeight
	self.array={unpack(self.arrayBak)}

	return self.divSum
end

-- main --
function main(x, y, p)

	local wMin, wMax=bs_width_min(), bs_width_max()
	local pp=1

	if pm[9].v > 0 then
		pp=p^(4*pm[9].v/100)*(1-wMin/wMax)+wMin/wMax
	end

	local w=wMax*pp

	if firstDraw then
		cornerSmooth=average.init()
		cornerLv=0
		counter=0
		lastX, lastY, lastP=x, y, pp
		lastRad={0, 0}
	end

	local skipFirstDraw=math.min(100, math.floor(wMax/4+0.5))
	local dx, dy=bs_dir()
	local rad={bs_atan(lastX-x, lastY-y), bs_atan(x-lastX, y-lastY), bs_atan(dx, dy)}

	if counter > skipFirstDraw then
		cornerSmooth:put(skipFirstDraw*1.5, math.abs(math.sin(lastRad[3]-rad[3]))/0.1)
		cornerSmooth:cut(math.floor(math.max(#cornerSmooth-(counter-skipFirstDraw-1), 0)*(1-p)))
		cornerLv=math.min(cornerSmooth:sum(2, 1-p)*pm[2].v/100, 1)^2
	end

	local distance=bs_distance(lastX-x, lastY-y)
	local interval=w*pm[1].v/100

	if counter > skipFirstDraw then
		interval=interval-interval*math.min(math.min(cornerLv^(1-p), 0.95), cornerLv/0.1)
	else
		interval=0
	end

	interval=math.max(interval*(lastP-pp+1), 1)

	if not firstDraw then
		if distance < interval then
			return 0
		end
	end

	local midX, midY=x-distance%interval*dx, y-distance%interval*dy
	local angle=math.pi*2*pm[6].v/100

	if pm[5].v == 1 then
		angle=angle+rad[3]
	end

	for i=0, math.max(math.floor(distance/interval*(1-pp)), 0) do

		local ww=w*math.random(100-pm[3].v, 100)/100
		angle=angle+bs_grand(0, math.pi*2*pm[7].v/100)

		local r, g, b=bs_fore()
		local a=255*bs_opaque()

		local limitT=0.95

		if p > limitT then
			a=(1-(p-limitT)/(1-limitT)*0.9)^(pm[8].v/100)*a
		end

		if pm[10].v > 0 then
			if p < limitT then
				a=(1/limitT*p)^(pm[10].v/100*8)*a
			end
		end

		a=math.min(255, a+a*(1-a/255)*cornerLv)

		local wLimit=1

		if w < wLimit then
			a=a*(pp^(wLimit-w/wLimit))
			ww=1
		end

		local rAngle=bs_grand(0, math.pi*2)
		local rDist=w*math.random(0, pm[4].v)/100

		local xx, yy=midX+math.cos(rAngle)*rDist, midY+math.sin(rAngle)*rDist
		local ddx, ddy=math.cos(rad[1]), math.sin(rad[1])

		xx=xx+ddx*interval*i
		yy=yy+ddy*interval*i

		local gw=math.floor(pp^(16/(ww+1))+0.5+math.min(ww/100, 2))

		if ww < 9 then
			gw=0
		elseif not multipleMode then
			angle=angle+math.random(0, 1)*math.pi/2
		end

		if counter >= skipFirstDraw then

			for jx=0, 0 do
				for jy=0, gw do

					local _sID=1

					if multipleMode then
						_sID=math.random(1, 3)
					end

					local _shape = shape[_sID]

					local id=1
					local code=''

					repeat

						code=cmdcon[_shape[id]]

						if (code=='z') then

							local www=ww/(gw+1)

							if gw > 0 then
								bs_bezier_mul(www/(98/(gw+1))*(math.random(0, 1)*-2+1), (www/90)*(math.random(0, 1)*-2+1))
							else
								bs_bezier_mul(www/(98/(gw+1)), www/90)
							end

							bs_bezier_move(0, (www-ww/100)*(jy-gw/2))
							bs_bezier_rotate(angle)
							bs_bezier_move(xx, yy)
							bs_fill(r, g, b, a*math.random(math.max(100*(1-pm[8].v/100*0.5), 100*(1-pp)^4),100)/100)

							id=id+1
						else
							id=(cmd[code] or cmd.default)(_shape, id+1)
						end

					until id > #_shape
				end
			end
		end
	end

	lastX, lastY, lastP=midX, midY, pp
	lastRad={bs_atan(lastX-x, lastY-y), bs_atan(x-lastX, y-lastY), bs_atan(dx, dy)}
	counter=counter+1
	firstDraw=false

	return 1
end

-- shape --
cmdcon={'b', 'm', 'c', 'l'}
cmdcon[0]='z'
cmd=cmd or {
	b=function(bz, i)
			bs_bezier_begin(bz[i], bz[i+1])
			return i+2
		end,
	m=function(bz, i)
			bs_bezier_m(bz[i], bz[i+1])
			return i+2
		end,
	c=function(bz, i)
			bs_bezier_c(bz[i], bz[i+1], bz[i+2], bz[i+3], bz[i+4], bz[i+5])
			return i+6
		end,
	l=function(bz, i)
			bs_bezier_l(bz[i], bz[i+1])
			return i+2
		end,
	default=function(bz, i)
			return  i
		end,
	}

shape={}
shape[1]={1, -39.24, 49.84, 3, -42.85, 48.21, -46.75, 43.45, -45.90, 39.09, 3, -48.25, 34.55, -42.93, 28.43, -48.56,
	26.06, 3, -50.50, 25.06, -49.95, 23.00, -49.98, 21.20, 3, -48.80, 16.88, -50.47, 12.48, -50.00, 7.91,
	3, -50.08, 3.98, -50.50, -0.74, -47.81, -2.71, 3, -50.13, -6.26, -50.82, -10.78, -46.38, -12.91, 3,
	-45.43, -16.62, -48.72, -19.42, -49.99, -22.49, 3, -49.99, -25.30, -49.99, -28.11, -49.99, -30.92, 3,
	-47.84, -33.43, -41.00, -33.78, -42.92, -38.38, 3, -44.65, -40.58, -47.45, -42.58, -44.69, -45.03, 3,
	-42.40, -47.89, -38.29, -50.84, -33.97, -49.98, 3, -30.27, -49.53, -28.27, -48.89, -24.66, -49.93, 3,
	-19.57, -49.05, -14.33, -49.86, -9.49, -49.35, 3, -7.42, -46.91, -8.08, -44.63, -8.44, -41.72, 3, -7.37,
	-38.41, -9.67, -34.34, -12.29, -31.92, 3, -14.46, -29.77, -6.31, -32.07, -5.49, -33.43, 3, -1.93, -36.06,
	-0.19, -39.54, 2.91, -42.35, 3, 0.46, -42.16, -2.20, -48.28, 2.86, -49.50, 3, 7.84, -50.22, 12.96, -49.80,
	17.98, -49.44, 3, 20.40, -49.50, 24.41, -50.75, 25.99, -48.84, 3, 25.58, -44.46, 20.76, -42.56, 19.55,
	-38.43, 3, 17.83, -36.96, 14.32, -36.64, 16.47, -33.49, 3, 16.60, -29.52, 8.23, -28.53, 11.82, -23.36,
	3, 13.49, -18.99, 18.67, -16.30, 16.45, -11.09, 3, 16.26, -6.86, 17.73, -1.71, 13.37, 1.52, 3, 9.99,
	4.89, 11.50, 11.30, 6.62, 13.10, 3, 4.29, 16.99, 10.57, 19.56, 9.92, 23.35, 3, 7.98, 26.45, 7.08, 29.23,
	10.94, 31.18, 3, 14.68, 34.66, 18.08, 25.65, 22.16, 27.98, 3, 27.11, 24.43, 21.44, 19.29, 23.87, 15.24,
	3, 23.71, 10.83, 23.98, 5.71, 27.91, 2.31, 3, 23.48, 0.45, 23.78, -5.22, 23.03, -8.11, 3, 20.60, -5.87,
	19.12, -12.70, 18.11, -14.26, 3, 16.74, -19.30, 26.56, -20.73, 20.77, -24.76, 3, 16.23, -26.85, 21.73,
	-30.69, 23.98, -31.93, 3, 28.68, -30.05, 27.54, -34.18, 24.95, -36.13, 3, 23.08, -39.95, 29.42, -41.61,
	30.30, -45.13, 3, 32.92, -49.23, 38.15, -52.12, 41.96, -47.34, 3, 46.30, -46.34, 47.19, -41.55, 49.87,
	-39.15, 3, 50.03, -31.93, 50.15, -24.68, 49.77, -17.46, 3, 49.27, -11.12, 40.00, -15.05, 40.95, -8.19,
	3, 40.82, -5.77, 42.15, -0.19, 45.24, -4.04, 3, 47.08, -6.21, 50.66, -3.61, 50.00, -1.22, 3, 50.05,
	1.75, 49.95, 4.88, 49.71, 7.71, 3, 49.94, 11.00, 49.87, 14.31, 49.99, 17.73, 3, 50.80, 20.53, 47.84,
	22.17, 45.33, 21.53, 3, 39.72, 21.56, 46.81, 24.96, 45.93, 26.92, 3, 44.30, 31.29, 40.42, 34.35, 36.61,
	36.77, 3, 33.80, 33.70, 30.91, 38.16, 34.90, 39.57, 3, 36.71, 42.78, 37.89, 47.20, 34.23, 49.70, 3,
	32.81, 50.48, 30.76, 49.78, 29.10, 49.98, 3, 26.19, 49.81, 23.07, 50.16, 20.30, 49.67, 3, 18.77, 47.52,
	17.96, 45.90, 16.15, 48.56, 3, 15.08, 50.27, 13.25, 50.03, 11.49, 49.96, 3, 5.84, 49.94, 0.18, 49.92,
	-5.47, 49.90, 3, -7.64, 47.95, -8.47, 45.22, -11.18, 44.19, 3, -11.56, 40.97, -11.52, 37.45, -11.19,
	34.18, 3, -14.24, 29.91, -17.58, 38.53, -19.70, 40.60, 3, -24.37, 40.62, -26.24, 46.08, -29.17, 49.04,
	3, -30.18, 50.76, -32.35, 49.73, -33.99, 49.97, 3, -35.74, 49.95, -37.49, 49.92, -39.24, 49.84, 2, -30.76,
	28.08, 3, -27.64, 24.32, -25.46, 19.38, -20.90, 17.23, 3, -18.36, 13.11, -13.68, 11.43, -11.56, 6.95,
	3, -10.47, 5.44, -13.55, 2.71, -10.40, 1.08, 3, -7.54, -0.49, -6.69, -2.44, -10.45, -3.23, 3, -15.74,
	-4.08, -15.43, -9.89, -18.30, -13.28, 3, -22.22, -16.61, -19.26, -20.66, -16.25, -23.36, 3, -17.67,
	-25.04, -14.77, -29.50, -18.47, -28.39, 3, -25.10, -29.88, -22.11, -23.18, -23.34, -19.86, 3, -22.54,
	-17.39, -23.59, -14.53, -23.09, -12.16, 3, -21.61, -10.16, -17.20, -8.09, -19.53, -5.40, 3, -22.03,
	-1.52, -29.49, -5.66, -28.59, 1.27, 3, -28.80, 5.45, -35.09, 6.61, -32.91, 10.83, 3, -31.26, 12.93,
	-35.54, 14.46, -33.50, 17.26, 3, -34.32, 20.92, -39.68, 21.79, -38.52, 26.64, 3, -37.83, 29.36, -32.15,
	29.38, -30.76, 28.08, 0}

shape[2] = { 1, -3.17, 49.96, 3, -7.53, 48.88, -11.79, 46.79, -14.86, 43.57, 3, -15.06, 39.31, -20.96, 36.04, -20.00,
	31.58, 3, -17.68, 28.75, -14.44, 24.26, -17.64, 21.80, 3, -22.89, 20.37, -23.59, 25.29, -22.33, 29.04,
	3, -22.82, 33.59, -23.91, 38.67, -22.19, 43.02, 3, -25.18, 46.95, -30.68, 47.25, -33.80, 43.25, 3, -38.35,
	39.42, -43.20, 35.89, -47.54, 31.87, 3, -48.98, 30.32, -49.62, 28.49, -49.28, 26.40, 3, -49.00, 24.29,
	-50.08, 21.70, -48.22, 20.14, 3, -47.02, 16.51, -39.69, 14.50, -43.87, 10.39, 3, -48.49, 7.20, -44.09,
	3.16, -40.65, 1.54, 3, -37.49, -0.50, -43.06, -0.71, -43.14, -3.54, 3, -47.65, -6.57, -45.53, -11.52,
	-46.46, -15.96, 3, -45.82, -20.73, -44.48, -26.00, -48.51, -29.91, 3, -50.44, -33.06, -49.80, -36.83,
	-49.91, -40.36, 3, -47.74, -43.86, -45.16, -47.20, -41.28, -48.94, 3, -39.24, -50.74, -36.43, -49.75,
	-33.96, -50.00, 3, -31.40, -50.49, -30.34, -47.26, -28.60, -45.99, 3, -25.44, -42.06, -32.18, -38.41,
	-29.06, -34.94, 3, -30.99, -32.03, -35.25, -28.50, -31.28, -25.44, 3, -31.90, -22.21, -25.52, -27.08,
	-25.47, -29.23, 3, -21.41, -32.20, -18.41, -36.23, -14.27, -39.17, 3, -13.19, -41.67, -9.45, -43.38,
	-12.21, -45.93, 3, -9.28, -51.03, -2.70, -49.77, 2.23, -50.00, 3, 7.22, -49.76, 12.35, -50.54, 17.24,
	-49.42, 3, 22.39, -48.02, 24.28, -41.04, 18.71, -38.78, 3, 12.63, -38.65, 20.89, -29.44, 13.50, -29.46,
	3, 9.55, -25.98, 13.36, -21.83, 15.21, -18.51, 3, 18.84, -16.35, 17.16, -11.86, 16.28, -8.57, 3, 18.61,
	-5.69, 16.79, -1.85, 14.88, 0.70, 3, 10.61, 2.97, 12.62, 9.34, 8.89, 12.07, 3, 4.06, 13.39, 8.62, 19.06,
	10.46, 21.82, 3, 11.32, 25.35, 5.39, 28.67, 10.82, 30.69, 3, 14.73, 35.37, 18.12, 25.55, 22.70, 27.74,
	3, 27.50, 24.05, 21.20, 18.75, 24.44, 14.55, 3, 23.28, 9.98, 25.37, 5.55, 28.10, 2.15, 3, 23.68, 0.57,
	24.07, -5.19, 23.48, -7.81, 3, 21.11, -5.70, 19.96, -11.81, 18.57, -13.23, 3, 15.97, -18.23, 25.18,
	-19.96, 22.06, -23.67, 3, 16.30, -25.84, 22.02, -33.28, 26.29, -31.67, 3, 30.56, -33.00, 20.36, -38.39,
	27.03, -40.82, 3, 29.20, -44.29, 32.91, -45.33, 37.20, -45.34, 3, 40.80, -42.69, 43.66, -39.31, 46.85,
	-36.30, 3, 50.15, -31.94, 49.83, -25.77, 48.52, -20.79, 3, 46.23, -17.40, 45.49, -14.16, 42.22, -11.29,
	3, 41.02, -8.49, 41.91, -5.91, 42.64, -3.77, 3, 43.93, -2.09, 51.63, -3.34, 49.91, 2.05, 3, 48.41, 6.90,
	50.46, 11.95, 48.60, 16.71, 3, 47.12, 19.59, 41.02, 22.50, 46.27, 25.45, 3, 47.46, 30.04, 41.76, 33.45,
	38.61, 36.22, 3, 36.26, 37.01, 31.39, 34.58, 32.97, 39.80, 3, 33.71, 41.75, 31.87, 42.82, 30.78, 44.13,
	3, 28.49, 47.38, 24.64, 48.22, 21.34, 50.00, 3, 13.17, 50.00, 5.00, 50.12, -3.17, 49.96, 2, -9.68, 18.48,
	3, -5.41, 17.04, -0.23, 13.60, 0.54, 9.01, 3, -0.31, 6.10, 0.62, 2.53, 0.32, 0.10, 3, -3.59, 0.34, -7.35,
	-0.13, -8.75, -4.39, 3, -11.62, -4.37, -11.30, -7.20, -11.21, -9.18, 3, -11.20, -11.20, -11.59, -13.35,
	-10.00, -14.92, 3, -9.15, -17.69, -4.65, -21.81, -6.67, -24.20, 3, -10.19, -20.70, -14.77, -17.75, -16.99,
	-13.05, 3, -18.27, -8.23, -23.46, -9.44, -25.55, -5.13, 3, -27.93, -1.15, -32.21, 1.81, -32.86, 6.64,
	3, -34.48, 9.00, -31.77, 11.64, -30.99, 8.04, 3, -26.66, 6.12, -23.65, 0.74, -19.04, 0.26, 3, -15.07,
	1.81, -8.45, -2.54, -7.64, 3.65, 3, -8.27, 7.77, -6.82, 11.32, -9.33, 15.22, 3, -10.19, 17.16, -14.50,
	19.90, -9.68, 18.48, 0 }

shape[3] = { 1, -34.01, 49.11, 3, -39.93, 47.03, -33.02, 39.17, -39.99, 37.46, 3, -42.28, 35.57, -45.77, 35.05, -43.84,
	31.05, 3, -42.83, 29.22, -39.06, 25.93, -40.25, 24.75, 3, -43.34, 22.02, -47.37, 18.10, -46.18, 13.46,
	3, -47.95, 9.16, -43.58, 3.56, -48.59, 1.27, 3, -51.07, -3.17, -49.55, -8.73, -50.00, -13.67, 3, -50.54,
	-18.53, -49.51, -23.04, -47.89, -27.40, 3, -49.27, -29.58, -48.96, -31.70, -47.76, -33.87, 3, -45.84,
	-37.14, -44.21, -39.74, -43.64, -43.16, 3, -41.16, -47.01, -36.42, -46.79, -33.21, -49.53, 3, -31.85,
	-50.56, -29.80, -49.69, -28.15, -49.94, 3, -26.54, -49.73, -24.37, -50.25, -23.10, -49.59, 3, -22.29,
	-45.56, -22.71, -41.36, -22.28, -37.27, 3, -19.63, -34.62, -16.70, -30.54, -21.56, -28.33, 3, -25.70,
	-29.61, -29.06, -27.34, -28.77, -22.76, 3, -29.81, -19.00, -35.19, -17.56, -32.39, -13.47, 3, -34.62,
	-11.26, -32.55, -8.12, -34.15, -5.51, 3, -37.48, -4.54, -40.81, 2.14, -36.12, 3.41, 3, -30.76, 5.48,
	-28.96, 0.16, -26.18, -3.56, 3, -22.39, -6.80, -19.31, -10.70, -15.08, -13.51, 3, -13.23, -16.38, -10.16,
	-18.44, -12.22, -21.64, 3, -11.18, -24.53, -4.62, -27.14, -10.97, -28.30, 3, -16.38, -29.69, -15.82,
	-35.85, -19.15, -39.47, 3, -23.04, -43.09, -17.09, -48.67, -13.55, -49.80, 3, -7.16, -50.18, -0.75,
	-49.91, 5.64, -49.99, 3, 7.94, -47.59, 9.00, -44.83, 12.05, -42.97, 3, 15.68, -43.21, 18.09, -48.65,
	22.13, -47.27, 3, 24.91, -51.17, 31.00, -50.25, 34.95, -49.39, 3, 32.32, -46.25, 38.27, -41.62, 35.07,
	-39.70, 3, 30.02, -37.85, 37.05, -35.41, 36.35, -31.92, 3, 37.76, -29.57, 36.02, -25.31, 33.31, -24.97,
	3, 31.01, -24.81, 27.88, -25.48, 26.12, -24.71, 3, 26.55, -20.02, 21.61, -18.25, 20.25, -14.30, 3, 18.58,
	-11.53, 14.70, -12.00, 16.56, -7.92, 3, 16.11, -4.35, 8.54, -3.18, 12.04, 1.73, 3, 13.85, 5.96, 18.53,
	8.61, 16.64, 13.68, 3, 14.77, 16.29, 17.71, 18.32, 16.79, 21.04, 3, 15.96, 25.34, 11.51, 26.94, 11.08,
	31.36, 3, 12.03, 37.03, 3.35, 37.16, 7.25, 42.38, 3, 9.85, 45.14, 11.56, 49.18, 6.46, 49.83, 3, 1.72,
	50.24, -3.17, 49.87, -7.95, 49.98, 3, -11.95, 50.71, -15.55, 49.03, -17.96, 46.72, 3, -22.69, 45.56,
	-24.41, 51.65, -29.40, 49.99, 3, -31.06, 49.98, -32.63, 50.24, -34.01, 49.11, 2, -9.99, 43.39, 3, -5.99,
	42.03, -0.88, 38.80, -0.07, 34.57, 3, -1.18, 31.87, 1.18, 26.42, -0.81, 24.98, 3, -4.91, 26.02, -7.47,
	23.42, -9.39, 20.56, 3, -12.12, 20.24, -11.43, 17.39, -11.52, 15.45, 3, -11.15, 12.53, -10.95, 4.57,
	-15.06, 9.68, 3, -17.67, 12.96, -19.66, 16.20, -23.50, 17.22, 3, -25.10, 19.67, -29.97, 23.75, -29.24,
	25.66, 3, -24.52, 23.89, -19.48, 25.99, -14.71, 25.39, 3, -9.30, 22.96, -6.79, 28.54, -8.40, 32.83,
	3, -6.98, 36.28, -9.52, 40.73, -12.27, 43.27, 3, -13.40, 44.34, -10.29, 43.35, -9.99, 43.39, 2, 24.94,
	49.38, 3, 21.91, 45.03, 23.75, 40.41, 23.92, 35.84, 3, 21.89, 31.42, 31.01, 28.06, 25.42, 25.26, 3,
	21.92, 24.16, 24.73, 17.07, 22.65, 17.47, 3, 20.33, 17.97, 18.24, 12.34, 18.03, 10.01, 3, 17.01, 5.64,
	25.63, 4.27, 21.01, 0.68, 3, 15.61, -2.62, 23.30, -8.19, 27.22, -7.15, 3, 26.56, -9.68, 21.56, -13.45,
	26.54, -15.82, 3, 29.11, -18.45, 31.32, -21.52, 33.98, -24.26, 3, 36.58, -26.31, 40.87, -23.96, 42.95,
	-21.83, 3, 47.50, -20.50, 49.03, -14.77, 49.84, -10.66, 3, 49.87, -4.21, 50.26, 2.31, 49.71, 8.71, 3,
	48.31, 11.18, 46.36, 13.05, 43.42, 12.09, 3, 39.78, 14.06, 40.78, 21.18, 43.29, 22.07, 3, 45.08, 19.60,
	50.02, 19.34, 50.00, 22.94, 3, 49.83, 25.38, 50.34, 27.98, 49.75, 30.31, 3, 48.28, 33.32, 46.81, 36.40,
	43.97, 38.30, 3, 41.31, 41.89, 40.20, 46.43, 36.11, 48.94, 3, 34.77, 50.83, 32.25, 49.69, 30.29, 49.99,
	3, 28.55, 49.79, 26.45, 50.43, 24.94, 49.38, 0 }
