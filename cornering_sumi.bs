-- cornering_sumi.bs --

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"interval", 3, 100, 80},
		{"cornering", 0, 100, 100},
		{"rnd_size", 0, 100, 25},
		{"rnd_pos", 0, 100, 5},
		{"move_dir", 0, 1, 1},
		{"angle", 0, 100, 0},
		{"rnd_angle", 0, 100, 0},
		{"rp_alpha", 0, 100, 0},
		{"p_size", 0, 100, 50},
		{"p_alpha", 0, 100, 25}
	}

	pm.ja={
		"間隔", "コーナリング", "rnd_サイズ", "rnd_位置", "進行方向",
		"角度", "rnd_角度", "rp_透明度", "p_サイズ", "p_透明度"
	}

	for i in ipairs(pm) do

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		_G[string.format('param%d', i)]=setParam(pm[i])
		pm[i].v=_G[string.format('bs_param%d', i)]()

	end

	default_size=setParam(30, 0)
	firstDraw=true
	bs_setmode(1)

-- sub --
average={
	init=function ()
		local obj={array={}, arrayBak={}, divSum=0, divWeight=0}
		setmetatable(obj, {__index=average})
		return obj
	end
}

function average:put(num, value)

	if #self.array == 0 then
		for i=1, math.max(num, 1) do
			self.array[i]=value
		end
	else
		table.insert(self.array, value)
		table.remove(self.array, 1)
	end

	self.arrayBak={unpack(self.array)}
end

function average:cut(num)
	if #self.array < 1 then return end
	if num > 0 then
		for i=1, math.min(num, #self.array-1) do
			table.remove(self.array, i)
		end
	end
end

function average:sum(mode, weight)

	if mode < 0 or mode > 2 then
		return nil
	end

	self.divSum, self.divWeight=0, 0

	for i, v in ipairs(self.array) do

		local _weight=1

		if mode == 1 then
			_weight=i
		elseif mode == 2 then
			_weight=i^weight
		end

		self.divSum=self.divSum+v*_weight
		self.divWeight=self.divWeight+_weight
	end

	self.divSum=self.divSum/self.divWeight
	self.array={unpack(self.arrayBak)}

	return self.divSum
end

-- main --
function main(x, y, p)

	local wMin, wMax=bs_width_min(), bs_width_max()
	local pp=1

	if pm[9].v > 0 then
		pp=p^(4*pm[9].v/100)*(1-wMin/wMax)+wMin/wMax
	end

	local w=wMax*pp

	if firstDraw then
		cornerSmooth=average.init()
		cornerLv=0
		counter=0
		lastX, lastY, lastP=x, y, pp
		lastRad={0, 0}
	end

	local skipFirstDraw=math.min(100, math.floor(wMax/4+0.5))
	local dx, dy=bs_dir()
	local rad={bs_atan(lastX-x, lastY-y), bs_atan(x-lastX, y-lastY), bs_atan(dx, dy)}

	if counter > skipFirstDraw then
		cornerSmooth:put(skipFirstDraw*1.5, math.abs(math.sin(lastRad[3]-rad[3]))/0.1)
		cornerSmooth:cut(math.floor(math.max(#cornerSmooth-(counter-skipFirstDraw-1), 0)*(1-p)))
		cornerLv=math.min(cornerSmooth:sum(2, 1-p)*pm[2].v/100, 1)^2
	end

	w=w+w/(2.5+cornerLv*p)*cornerLv

	local distance=bs_distance(lastX-x, lastY-y)
	local interval=w*pm[1].v/100

	if counter > skipFirstDraw then
		interval=interval-interval*math.min(math.min(cornerLv^(1-p), 0.95), cornerLv/0.1)
	else
		interval=0
	end

	interval=math.max(interval*(lastP-pp+1), 1)

	if not firstDraw then
		if distance < interval then
			return 0
		end
	end

	local midX, midY=x-distance%interval*dx, y-distance%interval*dy
	local angle=math.pi*2*pm[6].v/100

	if pm[5].v == 1 then
		angle=angle+rad[3]
	end

	for i=0, math.max(math.floor(distance/interval*(1-pp)), 0) do

		local ww=w*math.random(100-pm[3].v, 100)/100
		angle=angle+bs_grand(0, math.pi*2*pm[7].v/100)

		local r, g, b=bs_fore()
		local a=255*bs_opaque()

		local limitT=0.95

		if p > limitT then
			a=(1-(p-limitT)/(1-limitT)*0.9)^(pm[8].v/100)*a
		end

		if pm[10].v > 0 then
			if p < limitT then
				a=(1/limitT*p)^(pm[10].v/100*8)*a
			end
		end

		a=math.min(255, a+a*(1-a/255)*cornerLv)

		local wLimit=1

		if w < wLimit then
			a=a*(pp^(wLimit-w/wLimit))
			ww=1
		end

		local rAngle=bs_grand(0, math.pi*2)
		local rDist=w*math.random(0, pm[4].v)/100

		local xx, yy=midX+math.cos(rAngle)*rDist, midY+math.sin(rAngle)*rDist
		local ddx, ddy=math.cos(rad[1]), math.sin(rad[1])

		xx=xx+ddx*interval*i
		yy=yy+ddy*interval*i

		if counter >= skipFirstDraw then

			local id=1
			local code=''

			repeat

				code=cmdcon[shape[id]]

				if (code=='z') then

					bs_bezier_mul(ww/100, ww/100)
					bs_bezier_rotate(angle)
					bs_bezier_move(xx, yy)
					bs_fill(r, g, b, a*math.random(100-pm[8].v*0.75, 100)/100)

					id=id+1
				else
					id=(cmd[code] or cmd.default)(shape, id+1)
				end

			until id > #shape
		end
	end

	lastX, lastY, lastP=midX, midY, pp
	lastRad={bs_atan(lastX-x, lastY-y), bs_atan(x-lastX, y-lastY), bs_atan(dx, dy)}
	counter=counter+1
	firstDraw=false

	return 1
end

-- shape --
cmdcon={'b', 'm', 'c', 'l'}
cmdcon[0]='z'
cmd=cmd or {
	b=function(bz, i)
			bs_bezier_begin(bz[i], bz[i+1])
			return i+2
		end,
	m=function(bz, i)
			bs_bezier_m(bz[i], bz[i+1])
			return i+2
		end,
	c=function(bz, i)
			bs_bezier_c(bz[i], bz[i+1], bz[i+2], bz[i+3], bz[i+4], bz[i+5])
			return i+6
		end,
	l=function(bz, i)
			bs_bezier_l(bz[i], bz[i+1])
			return i+2
		end,
	default=function(bz, i)
			return  i
		end,
	}

shape={1, -27.30, -49.81, 3, -33.97, -49.88, -40.62, -49.51, -47.27, -48.32, 3, -52.47, -46.20, -49.27, -38.02,
	-44.18, -38.59, 3, -17.69, -38.93, 11.84, -34.37, 40.51, -37.69, 3, 53.95, -39.07, 56.05, -53.31, 32.74,
	-48.45, 3, 12.77, -45.59, -7.30, -49.63, -27.30, -49.81, 2, -10.45, -31.77, 3, -22.12, -31.81, -33.81,
	-31.57, -45.47, -30.87, 3, -48.70, -31.77, -50.41, -28.30, -50.00, -25.47, 3, -49.73, -17.38, -40.43,
	-19.37, -35.10, -18.71, 3, -9.65, -17.87, 15.84, -18.39, 41.29, -18.78, 3, 46.96, -17.44, 53.67, -26.08,
	47.89, -30.13, 3, 28.46, -30.83, 9.00, -31.69, -10.45, -31.77, 2, -47.11, -8.62, 3, -48.07, -8.59, -49.21,
	-7.66, -49.47, -6.66, 3, -50.08, -2.78, -47.14, 1.16, -43.68, 2.46, 3, -18.49, 5.55, 14.00, 2.35, 42.80,
	2.93, 3, 45.86, 2.75, 51.19, 1.37, 50.00, -2.83, 3, 48.40, -6.42, 43.02, -8.54, 39.88, -5.79, 3, 11.40,
	-9.51, -17.36, -3.00, -45.79, -7.53, 3, -46.02, -8.35, -46.53, -8.65, -47.11, -8.62, 2, -46.76, 8.72,
	3, -48.69, 8.69, -50.19, 11.99, -48.71, 13.45, 3, -14.63, 21.50, 9.51, 20.86, 37.43, 16.92, 3, 41.96,
	15.87, 50.76, 16.27, 49.87, 9.09, 3, 44.21, 9.95, 38.54, 10.65, 32.84, 11.19, 3, 6.62, 14.02, -19.84,
	12.60, -45.91, 8.96, 3, -46.20, 8.80, -46.49, 8.73, -46.76, 8.72, 2, -47.20, 25.94, 3, -47.97, 26.28,
	-48.65, 26.82, -49.12, 27.54, 3, -51.04, 32.02, -45.93, 36.91, -41.57, 35.86, 3, -20.91, 40.77, 19.83,
	36.59, 44.56, 36.79, 3, 48.39, 35.51, 51.98, 30.70, 49.50, 26.71, 3, 18.94, 28.88, -13.26, 26.22, -47.20,
	25.94, 2, 13.60, 42.49, 3, -6.40, 42.49, -26.46, 43.90, -46.45, 43.29, 3, -54.50, 43.31, -45.30, 53.84,
	-41.67, 49.81, 3, -15.32, 51.69, 11.04, 47.25, 37.36, 49.75, 3, 41.54, 50.89, 51.91, 51.78, 49.50, 44.75,
	3, 37.55, 43.00, 25.60, 42.49, 13.60, 42.49, 0}
